const path = require("path");
const express = require("express");
const fs = require("fs");
const authorize = require("./authorize");
const store = require("store");
const app = express();
const port = 8000;
const pool = require("./db");
app.use(express.json());

app.use(express.static(path.join(__dirname, "public")))
app.use(express.urlencoded({
    extended: true
}))

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    let a = store.get("username") || "login"
    if (a != "login") {
        a = a.userName
    }
    res.render("home", {
        username: a
    })
})

app.get('/login', (req, res) => {
    res.render("login")
})

app.get('/suit', [authorize], (req, res) => {
    res.render("suit")
})

app.get("/signup", (req, res) => {
    res.render("signup")
})

app.get("/profile", async (req, res) => {
    const query = await pool.query("SELECT user_game_id,user_name,user_password from user_games");
    res.render("profile", {
        data: query.rows
    })
})

app.delete("/profile", async (req, res) => {
    try {
        const {
            ID
        } = req.params;
        const deleteBiodata = await pool.query("DELETE FROM profile Where user_game_id = $1", [ID])

        res.json("deleted!")
    } catch (err) {
        console.error(err.message)
    }
})

app.post("/signup", async (req, res) => {
    const query = pool.query("SELECT id from user_games");
    try {
        const newSignuser = req.body["signuser"]
        const newSignpassword = req.body["signpassword"]
        const newSignup = await pool.query(
            "INSERT INTO user_games (user_name, user_password) VALUES ($1, $2)",
            [newSignuser, newSignpassword]
        );
        res.redirect("/login")
    } catch (err) {
        console.error(err.message);
    }
})

const getUser = async (req, res) => {
    try {
        const query = await pool.query('SELECT * FROM user_games WHERE user_name =$1 AND user_password = $2', [req.body["username"], req.body["password"]])
        if (query.rows.length == 1) {
            let user = query.rows[0].user_name
            store.set("username", {
                userName: user
            })
            res.redirect("/")
        } else {
            store.set("username", {
                userName: "NOT FOUND"
            })
            res.status(500).send({
                "error": "username/password doesnt match"
            })
        }
    } catch (err) {
        console.error(err.message)
    }
}

app.post("/login", (req, res) => {
    res.set("Content-Type", "application/json")
    getUser(req, res)
})

app.listen(port, () => console.log(port))